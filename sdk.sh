#!/bin/bash

# Source ROS Environment
source /opt/ros/indigo/setup.bash

# Build Workspace
catkin_make
catkin_make install

# Download Baxter SDK
cd src
wstool init .
wstool merge https://raw.githubusercontent.com/RethinkRobotics/baxter/master/baxter_sdk.rosinstall
wstool update

# Install Baxter SDK
cd ..
catkin_make
catkin_make install

# Download Baxter Script
wget https://github.com/RethinkRobotics/baxter/raw/master/baxter.sh
sudo chmod u+x baxter.sh

# Start Avahi
sudo service dbus start
sudo service avahi-daemon start
